import React, { useState } from 'react';
import './App.scss';
import Header from './components/Header/Header';
import MainBtnContainer from './pages/MainBntContainer/MainBtnContainer';
import Modal from './components/Modal/Modal';


// need to add states to btn, if this states wiil change we wiil chagne header & text 
function App() {

  const [header, setHeader] = useState('THe first Car Radio');
  const [text, setText] = useState(`The first car radio was invented in 1929 by Paul Galvin and was called the "Motorola" (Motorized Victrola).`);
  const [modalIsActive, setModalIsActive] = useState(false);
  const [name, setName] = useState('second');

  const handleContentChange = () => {
    if (name === 'second') {
      setHeader('The first engine');
      setText('The first engine in the world was invented by Thomas Newcomen in 1712.');
    } else {
      setHeader('The first Car Radio');
      setText(`The first car radio was invented in 1929 by Paul Galvin and was called the "Motorola" (Motorized Victrola).`);
    }
  };
 
  const handleCloseModal = () => {
    setModalIsActive(!modalIsActive);   
  };




  return (
    <div className="App">
      <Header />
      <MainBtnContainer handleCloseModal={handleCloseModal} handleContentChange={handleContentChange} setName={setName}/>
      <Modal modalIsActive={modalIsActive} text={text} header={header} handleCloseModal={handleCloseModal} /> 
    </div>
  );
}

export default App;
