import React from 'react';
import styles from './MainBtnContainer.module.scss';
import Button from '../../components/Button/Button';


function MainBtnContainer({ handleCloseModal, setName, handleContentChange }) {


    const handleButtonClick = (newName) => {
        setName(newName);
        handleContentChange();
        handleCloseModal();
      };

    return (
        <div className={styles.root}>
        <Button onClick={() => handleButtonClick('first')} background="rgb(112, 126, 232)" children="Open First Modal" />
        <Button onClick={() => handleButtonClick('second')} background="rgb(191, 132, 132)" children="Open Second Modal" />
        </div>
    );
  }
  
  export default MainBtnContainer;
