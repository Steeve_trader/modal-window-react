import classNames from "classnames";
import React from "react";
import styles from "./Modal.module.scss";


function Modal({ modalIsActive, text, header, handleCloseModal }) {
    
    return (
      <div className={classNames( styles.root, { [styles.isActive]: modalIsActive } )}>
        <div className={styles.background} onClick={handleCloseModal}>
          <div className={styles.mainContainer} >
            <button className={classNames(styles.closeBtn, styles.closeBtn.active)} onClick={handleCloseModal}>{`X`}</button>
            <h1>{header}</h1>
            <p>{text}</p>
          </div>
        </div>
      </div>
    );
  }
  
  export default Modal;
