import React from 'react';
import styles from './Button.module.scss';


function Button({ onClick, children, background}) {

    
    
    return (
      <div className={styles.root}>
        <button onClick={onClick} style={{ backgroundColor: background }} className={styles.btn}>{children}</button>
      </div>
    );
  }
  
  export default Button;
